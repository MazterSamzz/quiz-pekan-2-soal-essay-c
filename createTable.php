CREATE TABLE customers (
        id int primary key AUTO_INCREMENT,
        name varchar(255),
        email varchar(255),
        password varchar(255)
    );
CREATE TABLE orders(
        id int primary key AUTO_INCREMENT,
        amount varchar(255),
        customer_id int,
        FOREIGN KEY (customer_id) REFERENCES customers(id)
    );